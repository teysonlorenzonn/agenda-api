
const regString = /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/;
const regInteiros = /[0-9]+$/;
const regEmail = /^[a-zA-Z0-9.!#$%&'*+=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
const regData = /^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|11)(-)(0[1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|[12][0-9]|2[0-8]))|(([02468][048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(02)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02)(-)(29)))(\s)(([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$/;


export const hasValidString = strin => regString.test(strin);
export const hasValidInteiro = intei => regInteiros.test(intei);
export const hasValidEmail = email => regEmail.test(email);
export const hasValidData = data => regData.test(data);


export const isEmpty = variavel => {
    if(variavel !== '' && variavel !== null && variavel !== 'null' && variavel !== undefined){
        return false;
    }
    return true;
}

export const hasValidCpf = cpf => {
    
    if(cpf === 'not') return true;
    
    let soma;
    let resto;

    let strCPF = normalizeCpf(cpf);

    if(!hasValidInteiro(strCPF)) return false;

    soma = 0;
    if (strCPF == "00000000000") return false;

    for (let i = 1; i <= 9; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11)) resto = 0;
    if (resto != parseInt(strCPF.substring(9, 10))) return false;

    soma = 0;
    for (let i = 1; i <= 10; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11)) resto = 0;
    if (resto != parseInt(strCPF.substring(10, 11))) return false;
    return true;
};

export const normalizeCpf = cpf => {
    let strCPF = '';

    for(let i = 0; i < cpf.length; i++) {
        if (regInteiros.test(cpf.charAt(i))) {
            strCPF += cpf.charAt(i);
        }
    }

    return strCPF;
}

export const hasComparaDatas = (dataI, dataF) => {
    
    dataI = dataI.split(' ');
    dataF = dataF.split(' ');
    let horaI = dataI[1].replace(/:/g,'');
    dataI = dataI[0].replace(/-/g,'');
    let horaF = dataF[1].replace(/:/g,'');
    dataF = dataF[0].replace(/-/g,'');

    if(parseInt(dataI) > parseInt(dataF)){
        return [false, 'erro data final e menor do que a data inicial'];
    } 
    if(parseInt(dataI) === parseInt(dataF)){
        
        if(parseInt(horaI) > parseInt(horaF)){
           
            return [false, 'erro hora final e menor do que a hora inicial'];
        }
    }
    return [true,'sucesso'];
}