import jwt from 'jsonwebtoken';
import { isEmpty } from './Validacao.mjs';

export const verifyJWT = (req, res, next) => {
  
  let token = req.headers['x-access-token'];
  jwtToken(token,req, res, next, process.env.SECRET);
}

const jwtToken = (token,req, res, next, envSecret) => {
  if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
  jwt.verify(token, envSecret, (err, decoded) => {
    if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
    // se tudo estiver ok, salva no request para uso posterior
    req.userId = decoded.id;
    next();
  });
}