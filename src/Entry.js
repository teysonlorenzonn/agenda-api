require = require("@std/esm")(module);
dotenv = require('dotenv')
dotenv.config();

module.exports = require("./App.mjs").default;
