import Consultas from '../database/Consultas.mjs';
import { hasValidString, hasValidCpf, hasValidEmail, normalizeCpf } from '../utils/Validacao.mjs';


class Pessoa {

    constructor(id, nome, sobrenome, cpf, email) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.cpf = cpf;
        this.email = email;
        this.msgError = [];
    }

    getId() {
        return this.id;
    }
    getNome() {
        return this.nome;
    }
    getSobrenome() {
        return this.sobrenome;
    }
    getCpf() {
        return this.cpf;
    }
    getEmail() {
        return this.email;
    }
    getNomeCompleto() {
        return `${this.nome} ${this.sobrenome}`;
    }
    getMsgError(){
        return this.msgError;
    }

    setNome(novoNome) {
        this.nome = novoNome;
    }
    setSobrenome(novoSobrenome) {
        this.sobrenome = novoSobrenome;
    }
    setCpf(novoCpf) {
        this.cpf = novoCpf;
    }
    setEmail(novoEmail) {
        this.email = novoEmail;
    }
    setMsgError(msg){
        this.msgError.push(msg);
    }

    validacaoPessoa(){
        
        hasValidEmail(this.getEmail()) ? true : this.msgError.push('erro email invalido');
        hasValidString(this.getNome()) ? true : this.msgError.push('erro nome invalido') ;
        hasValidString(this.getSobrenome()) ? true : this.msgError.push('erro sobrenome invalido');
        hasValidCpf(this.getCpf()) ? true : this.msgError.push('erro cpf invalido');
        
        return this.msgError.length > 0 ? false : true; 
    }

    static getPessoasPorId(res, id) {
        let query = `select * from pessoa where id_pessoa = ${id}`;
        Consultas.getAll(res, query,'pessoa');
    }

    static getAllPessoas(res) {
        let query = 'select * from pessoa';
        Consultas.getAll(res, query,'pessoa');
    }

    static getPessoasPorNome(res,nome){
        let query = `select * from pessoa where concat(nome_pessoa,' ',sobrenome_pessoa) like '%${nome}%'`;
        Consultas.getAll(res, query, 'pessoa');
    }

    static updatePes(res, updatePessoa){
        let quantQuery = 2;
        let query = [];
        let cpf =  normalizeCpf(updatePessoa.getCpf());

        let queryUm = `update pessoa set nome_pessoa = '${updatePessoa.getNome()}', sobrenome_pessoa = '${updatePessoa.getSobrenome()}', ` +
        `cpf_pessoa = '${cpf}', email_pessoa = '${updatePessoa.getEmail()}' ` +
        `where id_pessoa = ${updatePessoa.getId()}`;
        
        let queryDois = `update usuario set login_usuario = '${updatePessoa.getEmail()}' ` +
        `where id_usuario = ${updatePessoa.getId()}`;

        query.push(queryUm,queryDois);
        Consultas.putAll(res, query, quantQuery, 'pessoa');;

    }
}

export default Pessoa;