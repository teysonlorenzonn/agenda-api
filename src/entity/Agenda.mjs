import Consultas from '../database/Consultas.mjs';
import {hasValidString, hasValidInteiro} from '../utils/Validacao.mjs';

class Agenda{

    constructor(idAgenda,nome,tipo,idUser = 1){
        this.idAgenda = idAgenda;
        this.nome = nome;
        this.tipo = tipo;
        this.idUser = idUser;
        this.msgError = []
  
    }

    getIdUser(){
        return this.idUser;
    }
    getIdAgenda(){
        return this.idAgenda;
    }
    getNome(){
        return this.nome;
    }
    getTipo(){
        return this.tipo;
    }
    getMsgError(){
        return this.msgError;
    }

    setNome(novoNome){
        this.nome = novoNome;
    }
    setTipo(novaTipo){
        this.tipo = novaTipo;
    }

    setMsgError(msg){
        this.msgError.push(msg);
    }

    validacaoAgenda(){
   
        hasValidString(this.getNome()) ? true : this.setMsgError('erro nome invalido')
        hasValidString(this.getTipo()) ? true : this.setMsgError('erro tipo invalido')
        hasValidInteiro(this.getIdAgenda()) ? true : this.setMsgError('erro id invalido');
        hasValidInteiro(this.getIdUser()) ? true : this.setMsgError('erro id_user invalido');
        
        return this.msgError.length > 0 ? false : true; 
          
    }

    
    static postNewAgenda(res,novaAgenda){
        let quantQuery = 1;
        let query = [];
        
        let queryUm = 'insert into agenda ( nome_agenda, tipo_agenda, id_usuario ) ' +
        `values('${novaAgenda.getNome()}','${novaAgenda.getTipo()}', '${novaAgenda.getIdUser()}')`;

        query.push(queryUm);

        Consultas.postAll(res, query, quantQuery, 'agenda');

    }


    static deleteAgenda(res,deleteAgenda){
        let quantQuery = 2;
        let query = [];
        
        let queryUm = 'delete from pagina ' +
        `where id_agenda = '${deleteAgenda.getIdAgenda()}'`

        let queryDois = 'delete from agenda ' +
        `where id_agenda=${deleteAgenda.getIdAgenda()}`;

        query.push(queryUm,queryDois);

        Consultas.deleteAll(res, query, quantQuery, 'agenda');
    }

    static getAgendaPorId(res, id) {

        let query = 'select a.id_agenda, a.nome_agenda, a.tipo_agenda, u.id_usuario, u.login_usuario from agenda as a ' + 
        'inner join usuario as u on a.id_usuario = u.id_usuario ' +
        `where id_agenda=${id}`;
        
        Consultas.getAll(res,query,'agenda');

    }

    static getAllAgenda(res) {
        
        let query = 'select a.id_agenda, a.nome_agenda, a.tipo_agenda, u.id_usuario, u.login_usuario from agenda as a ' + 
        'inner join usuario as u on a.id_usuario = u.id_usuario';

        Consultas.getAll(res,query,'agenda');
    }

    static getAgendaPorNome(res,nome){
        
        let query = 'select a.id_agenda, a.nome_agenda, a.tipo_agenda, u.id_usuario, u.login_usuario from agenda as a ' + 
        'inner join usuario as u on a.id_usuario = u.id_usuario ' +
        `where nome_agenda like '%${nome}%'`;
        
        Consultas.getAll(res, query,'agenda');
    }

    static getAgendaPorTipo(res,tipo){
        
        let query = 'select a.id_agenda, a.nome_agenda, a.tipo_agenda, u.id_usuario, u.login_usuario from agenda as a ' + 
        'inner join usuario as u on a.id_usuario = u.id_usuario ' +
        `where tipo_agenda like '%${tipo}%'`;
        
        Consultas.getAll(res, query,'agenda');
    }


    static updateAgenda(res, updateAgenda){
        let quantQuery = 1;
        let query = [];

        let queryUm = `update agenda set nome_agenda = '${updateAgenda.getNome()}', tipo_agenda = '${updateAgenda.getTipo()}' ` + 
        `where id_agenda = ${updateAgenda.getIdAgenda()}`;
        
        query.push(queryUm);
        Consultas.putAll(res, query, quantQuery, 'agenda');;

    }

}

export default Agenda;