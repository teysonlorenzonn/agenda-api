import Consultas from '../database/Consultas.mjs';
import Agenda from '../entity/Agenda.mjs';
import {hasValidString, hasValidInteiro, hasValidData, hasComparaDatas} from '../utils/Validacao.mjs';

class Paginas extends Agenda{
   
    constructor(idAgenda,nomeAgenda,tipoAgenda,idUser,idPagina,nome,descricao,dataInicial,dataFinal){
        super(idAgenda,nomeAgenda,tipoAgenda,idUser);
        this.idPagina = idPagina;
        this.nome = nome;
        this.descricao = descricao;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
    }

    getIdPagina(){
        return this.idPagina;
    }
    getNome(){
        return this.nome;
    }
    getDescricao(){
        return this.descricao
    }
    getDataInicial(){
        return this.dataInicial
    }
    getDataFinal(){
        return this.dataFinal

    }

    
    setNome(novoNome){
        this.nome = novoNome;
    }
    setDescricao(novaDescricao){
        this.descricao = novaDescricao;
    }
    setDataInicial(novaDataInicial){
        this.dataInicial = novaDataInicial;
    }
    setDataFinal(novaDataFinal){
        this.dataFinal = novaDataFinal;
    }

    validacaoPagina(){
   
        hasValidString(this.getNome()) ? true : super.setMsgError('erro nome invalido')
        hasValidInteiro(this.getIdPagina()) ? true : super.setMsgError('erro id invalido');
        hasValidData(this.getDataInicial()) ? true : super.setMsgError('erro data inicial invalida');
        hasValidData(this.getDataFinal()) ? true : super.setMsgError('erro data final invalida');
        const verify = hasComparaDatas(this.getDataInicial(), this.getDataFinal());
        verify[0] ? true : super.setMsgError(verify[1]);
        
        return super.validacaoAgenda();
          
    }


    static postNewPage(res,novaPage){
        let quantQuery = 1;
        let query = [];
        
        
        let queryUm = 'insert into pagina ( nome_pagina, descricao_pagina, data_inicial_pagina, data_final_pagina, id_agenda ) ' +
        `values('${novaPage.getNome()}','${novaPage.getDescricao()}','${novaPage.getDataInicial()}','${novaPage.getDataFinal()}','${novaPage.getIdAgenda()}')`;

        query.push(queryUm);

        Consultas.postAll(res, query, quantQuery, 'pagina');

    }

    static deletePage(res,deletePage){
        let quantQuery = 1;
        let query = [];
        
        let queryUm = 'delete from pagina ' +
        `where id_pagina=${deletePage.getIdPagina()}`;

        query.push(queryUm);

        Consultas.deleteAll(res, query, quantQuery, 'pagina');
    }

    static getPagePorId(res, id) {

        let query = 'select p.id_pagina, p.nome_pagina, p.descricao_pagina, p.data_inicial_pagina, p.data_final_pagina, ' +
        'a.id_agenda, a.nome_agenda, a.tipo_agenda, u.id_usuario, u.login_usuario ' +
        'from pagina as p ' +
        'inner join agenda as a on p.id_agenda = a.id_agenda ' +
        'inner join usuario as u on a.id_usuario = u.id_usuario ' +
        `where p.id_pagina=${id}`;
        
        Consultas.getAll(res,query,'pagina');

    }

    static getAllpage(res) {
        
        let query = 'select p.id_pagina, p.nome_pagina, p.descricao_pagina, p.data_inicial_pagina, p.data_final_pagina, ' +
        'a.id_agenda, a.nome_agenda, a.tipo_agenda, u.id_usuario, u.login_usuario ' +
        'from pagina as p ' +
        'inner join agenda as a on p.id_agenda = a.id_agenda ' +
        'inner join usuario as u on a.id_usuario = u.id_usuario';

        Consultas.getAll(res, query, 'pagina');
    }

    static getPagePorNome(res,nome){
        
        let query = 'select p.id_pagina, p.nome_pagina, p.descricao_pagina, p.data_inicial_pagina, p.data_final_pagina, ' +
        'a.id_agenda, a.nome_agenda, a.tipo_agenda, u.id_usuario, u.login_usuario ' +
        'from pagina as p ' +
        'inner join agenda as a on p.id_agenda = a.id_agenda ' +
        'inner join usuario as u on a.id_usuario = u.id_usuario ' +
        `where p.nome_pagina like '%${nome}%'`;
        
        Consultas.getAll(res, query, 'pagina');
    }


    static updatePage(res, updatePage){
        let quantQuery = 1;
        let query = [];

        
        let queryUm = `update pagina set nome_pagina = '${updatePage.getNome()}', descricao_pagina = '${updatePage.getDescricao()}', data_inicial_pagina = '${updatePage.getDataInicial()}', data_final_pagina = '${updatePage.getDataFinal()}', id_agenda = ${updatePage.getIdAgenda()} ` + 
        `where id_pagina = ${updatePage.getIdPagina()}`;
        
        query.push(queryUm);
        Consultas.putAll(res, query, quantQuery, 'pagina');;

    }
}

export default Paginas;