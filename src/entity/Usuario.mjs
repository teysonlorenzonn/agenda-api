import Pessoa from './Pessoa.mjs';
import Consultas from '../database/Consultas.mjs';
import { hasValidInteiro, hasValidEmail, normalizeCpf } from '../utils/Validacao.mjs';


class Usuario extends Pessoa {

    constructor(idPessoa, nomePessoa, sobrenomePessoa, cpfPessoa, emailPessoa, id, senha, nivel, ativo) {
        super(idPessoa, nomePessoa, sobrenomePessoa, cpfPessoa, emailPessoa);
        this.id = id;
        this.login = emailPessoa;
        this.senha = senha;
        this.nivel = nivel;
        this.ativo = ativo;

    }

    getId() {
        return this.id;
    }
    getLogin() {
        return this.login;
    }
    getSenha() {
        return this.senha;
    }
    getNivel() {
        return this.nivel;
    }
    getAtivo(){
        return this.ativo;
    }
    getMsgError() {
        return this.msgError;
    }

    setLogin(novologin) {
        this.login = novoLogin;
    }
    setSenha(novoSenha) {
        this.senha = novoSenha;
    }
    setNivel(novoNivel) {
        this.nivel = novoNivel;
    }
    setAtivo(ativo) {
        this.ativo = ativo;
    }


    validacaoUser() {


        hasValidInteiro(this.getNivel()) ? true : super.setMsgError('erro nivel invalido');
        hasValidEmail(this.getLogin()) ? true : super.setMsgError('erro login invalido');
        hasValidInteiro(this.getId()) ? true : super.setMsgError('erro id invalido');
        hasValidInteiro(this.getAtivo()) ? true : super.setMsgError('erro ativo invalido');

        return super.validacaoPessoa();

    }

    static postNewUser(res, novoUsuario) {
        let quantQuery = 2;
        let query = [];
        let cpf = normalizeCpf(novoUsuario.getCpf());

        let queryUm = 'insert into pessoa ( nome_pessoa, sobrenome_pessoa, cpf_pessoa, email_pessoa ) ' +
            `values('${novoUsuario.getNome()}','${novoUsuario.getSobrenome()}','${cpf}','${novoUsuario.getEmail()}')`;

        let queryDois = 'insert into usuario ( login_usuario, senha_usuario, nivel_usuario , id_pessoa , ativo_usuario ) ' +
            `values('${novoUsuario.getEmail()}','${novoUsuario.getSenha()}',${novoUsuario.getNivel()}, @@IDENTITY, ${novoUsuario.getAtivo()} )`;

        query.push(queryUm, queryDois);

        Consultas.postAll(res, query, quantQuery, 'user');

    }

    static deleteUser(res, deleteUsuario) {
        let quantQuery = 1;
        let query = [];

        let queryUm = `update usuario set ativo_usuario = ${deleteUsuario.getAtivo()} ` +
            `where id_usuario=${deleteUsuario.getId()}`;


        query.push(queryUm);

        Consultas.putAll(res, query, quantQuery, 'user');
    }

    static getUsuarioPorId(res, id) {

        let query = 'select u.id_usuario, u.login_usuario, u.senha_usuario, u.nivel_usuario, ' +
            'p.id_pessoa, p.nome_pessoa, p.sobrenome_pessoa, p.cpf_pessoa, p.email_pessoa ' +
            'from usuario as u ' +
            'inner join pessoa as p on u.id_pessoa = p.id_pessoa ' +
            `where u.id_usuario=${id}`;

        Consultas.getAll(res, query, 'user');

    }

    static getAllUsuarios(res) {

        let query = 'select u.id_usuario, u.login_usuario, u.ativo_usuario, u.nivel_usuario, ' +
            'p.id_pessoa, p.nome_pessoa, p.sobrenome_pessoa, p.cpf_pessoa, p.email_pessoa ' +
            'from usuario as u ' +
            'inner join pessoa as p on u.id_pessoa = p.id_pessoa';

        Consultas.getAll(res, query, 'user');
    }

    static getLoginUsuario(res, login, senha) {

        let query = 'select u.id_usuario, u.login_usuario, u.ativo_usuario, u.senha_usuario, u.nivel_usuario, ' +
            'p.id_pessoa, p.nome_pessoa, p.sobrenome_pessoa, p.cpf_pessoa, p.email_pessoa ' +
            'from usuario as u ' +
            'inner join pessoa as p on u.id_pessoa = p.id_pessoa ' +
            `where u.login_usuario='${login}'`;
        Consultas.getAll(res, query, 'user-login', login, senha);
    }

    static getUsuarioPorLogin(res, login) {
        let query = 'select u.id_usuario, u.login_usuario, u.ativo_usuario, u.nivel_usuario, ' +
            'p.id_pessoa, p.nome_pessoa, p.sobrenome_pessoa, p.cpf_pessoa, p.email_pessoa ' +
            'from usuario as u ' +
            'inner join pessoa as p on u.id_pessoa = p.id_pessoa ' +
            `where u.login_usuario like '%${login}%'`;

        Consultas.getAll(res, query, 'user');
    }

    static updateUser(res, updateUsuario) {
        let quantQuery = 2;
        let query = [];
        let cpf = normalizeCpf(updateUsuario.getCpf());

        let queryUm = `update pessoa set email_pessoa = '${updateUsuario.getEmail()}', nome_pessoa = '${updateUsuario.getNome()}', sobrenome_pessoa = '${updateUsuario.getSobrenome()}', cpf_pessoa = '${cpf}' ` +
            `where id_pessoa = ${updateUsuario.getId()}`;

        let queryDois = `update usuario set login_usuario = '${updateUsuario.getEmail()}', ` +
            `senha_usuario = '${updateUsuario.getSenha()}', nivel_usuario = ${updateUsuario.getNivel()}, ativo_usuario = ${updateUsuario.getAtivo()} ` +
            `where id_usuario = ${updateUsuario.getId()}`;

        query.push(queryUm, queryDois);
        Consultas.putAll(res, query, quantQuery, 'user');;

    }
}

export default Usuario;