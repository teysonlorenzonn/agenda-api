import express from 'express';
import Usuario from '../entity/Usuario.mjs';
import Pessoa from '../entity/Pessoa.mjs';
import Agenda from '../entity/Agenda.mjs';
import Pagina from '../entity/Pagina.mjs';

import { verifyJWT } from '../utils/VerificaAuth.mjs';
import { isEmpty } from '../utils/Validacao.mjs';


const router = express.Router();

router.get('/', (req, res, next) => {
    let info = {
        'Server': 'https://agenda-rest-api.herokuapp.com',
        'Status': 'online'
    };
    res.status(200).json(info);
});

router.post('/login', (req, res, next) => {

    try {
        let login = req.body.login;
        let senha = req.body.senha;

        isEmpty(login) && res.status(400).json({ 'erro': `por favor envie o parametro como login:` })
        isEmpty(senha) && res.status(400).json({ 'erro': `por favor envie o parametro como senha:` })

        Usuario.getLoginUsuario(res, login, senha);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.post('/usuarios', (req, res, next) => {

    try {
        let email = req.body.email;

        let senha = req.body.senha;
        let nivel = req.body.nivel;
        let nomePessoa = req.body.info_user.nome;
        let sobrenomePessoa = req.body.info_user.sobrenome;
        let cpfPessoa = req.body.info_user.cpf;


        isEmpty(email) && res.status(400).json({ 'erro': `por favor envie o parametro como email:` })
        isEmpty(senha) && res.status(400).json({ 'erro': `por favor envie o parametro como senha:` })
        isEmpty(nivel) && res.status(400).json({ 'erro': `por favor envie o parametro como nivel:` })
        isEmpty(nomePessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{nome:}` })
        isEmpty(sobrenomePessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{sobrenome:}` })
        isEmpty(cpfPessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{cpf:}` })

        const novoUsuario = new Usuario(0, nomePessoa, sobrenomePessoa, cpfPessoa, email, 0, senha, nivel, 1);
        let confirma = novoUsuario.validacaoUser();
        confirma ? Usuario.postNewUser(res, novoUsuario) : res.status(200).json({ 'erros': novoUsuario.getMsgError() });;
    }
    catch (e) {
        res.status(400).json({ 'erro': `por favor para levar os dados da pessoa envie o parametro info_user:{nome:,sobrenome:,cpf:}` })
    }

});

router.post('/agendas', verifyJWT, (req, res, next) => {

    try {
        let nome = req.body.nome;
        let tipo = req.body.tipo;
        let id = req.body.id_user;

        isEmpty(nome) && res.status(400).json({ 'erro': `por favor envie o parametro como nome:` })
        isEmpty(tipo) && res.status(400).json({ 'erro': `por favor envie o parametro como tipo:` })
        isEmpty(id) && res.status(400).json({ 'erro': `por favor envie o parametro como id_user:` })

        const novaAgenda = new Agenda(0, nome, tipo, id);
        let confirma = novaAgenda.validacaoAgenda();
        confirma ? Agenda.postNewAgenda(res, novaAgenda) : res.status(200).json({ 'erros': novaAgenda.getMsgError() });;
    }
    catch (e) {
        res.status(400).json({ 'erro': `por favor para levar os dados da pessoa envie o parametro info_user:{nome:,sobrenome:,cpf:}` })
    }

});

router.post('/paginas', verifyJWT, (req, res, next) => {
    try {


        let nome = req.body.nome;
        let descricao = req.body.descricao;
        let dataInicio = req.body.data_inicio;
        let dataFinal = req.body.data_final;
        let idAgenda = req.body.id_agenda;

        isEmpty(nome) && res.status(400).json({ 'erro': `por favor envie o parametro como nome:` })
        isEmpty(descricao) && res.status(400).json({ 'erro': `por favor envie o parametro como descricao:` })
        isEmpty(dataInicio) && res.status(400).json({ 'erro': `por favor envie o parametro como data_inicio:` })
        isEmpty(dataFinal) && res.status(400).json({ 'erro': `por favor envie o parametro como data_final:` })
        isEmpty(idAgenda) && res.status(400).json({ 'erro': `por favor envie o parametro como id_agenda:` })

        const novaPagina = new Pagina(idAgenda, 'not', 'not', idAgenda, 1, nome, descricao, dataInicio, dataFinal);
        let confirma = novaPagina.validacaoPagina();
        confirma ? Pagina.postNewPage(res, novaPagina) : res.status(200).json({ 'erros': novaPagina.getMsgError() });;
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.put('/agendas', verifyJWT, (req, res, next) => {

    try {

        let id = req.body.id;
        let nome = req.body.nome;
        let tipo = req.body.tipo;
        isEmpty(id) && res.status(400).json({ 'erro': `por favor envie o parametro como id:` })
        isEmpty(nome) && res.status(400).json({ 'erro': `por favor envie o parametro como nome:` })
        isEmpty(tipo) && res.status(400).json({ 'erro': `por favor envie o parametro como tipo:` })

        const updateAgenda = new Agenda(id, nome, tipo);
        let confirma = updateAgenda.validacaoAgenda();
        confirma ? Agenda.updateAgenda(res, updateAgenda) : res.status(200).json({ 'erros': updateAgenda.getMsgError() });
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });

    }

});

router.put('/paginas', verifyJWT, (req, res, next) => {

    try {
        let id = req.body.id;
        let nome = req.body.nome;
        let descricao = req.body.descricao;
        let dataInicio = req.body.data_inicio;
        let dataFinal = req.body.data_final;
        let idAgenda = req.body.id_agenda;

        isEmpty(id) && res.status(400).json({ 'erro': `por favor envie o parametro como id:` })
        isEmpty(nome) && res.status(400).json({ 'erro': `por favor envie o parametro como nome:` })
        isEmpty(descricao) && res.status(400).json({ 'erro': `por favor envie o parametro como descricao:` })
        isEmpty(dataInicio) && res.status(400).json({ 'erro': `por favor envie o parametro como data_inicio:` })
        isEmpty(dataFinal) && res.status(400).json({ 'erro': `por favor envie o parametro como data_final:` })
        isEmpty(idAgenda) && res.status(400).json({ 'erro': `por favor envie o parametro como id_agenda:` })

        const updatePagina = new Pagina(idAgenda, 'not', 'not', idAgenda, id, nome, descricao, dataInicio, dataFinal);
       
        let confirma = updatePagina.validacaoPagina();
        confirma ? Pagina.updatePage(res, updatePagina) : res.status(200).json({ 'erros': updatePagina.getMsgError() });;
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.put('/usuarios', verifyJWT, (req, res, next) => {
    try {

        let id = req.body.id;
        let email = req.body.email;
        let ativo = req.body.ativo;
        let senha = req.body.senha;
        let nivel = req.body.nivel;
        let nomePessoa = req.body.info_user.nome;
        let sobrenomePessoa = req.body.info_user.sobrenome;
        let cpfPessoa = req.body.info_user.cpf;


        isEmpty(email) && res.status(400).json({ 'erro': `por favor envie o parametro como email:` })
        isEmpty(senha) && res.status(400).json({ 'erro': `por favor envie o parametro como senha:` })
        isEmpty(nivel) && res.status(400).json({ 'erro': `por favor envie o parametro como nivel:` })
        isEmpty(nomePessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{nome:}` })
        isEmpty(sobrenomePessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{sobrenome:}` })
        isEmpty(cpfPessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{cpf:}` })
        isEmpty(ativo) && res.status(400).json({ 'erro': `por favor envie o parametro como ativo:` })

        const updateUsuario = new Usuario(id, nomePessoa, sobrenomePessoa, cpfPessoa, email, id, senha, nivel, ativo);
        let confirma = updateUsuario.validacaoUser();
        confirma ? Usuario.updateUser(res, updateUsuario) : res.status(200).json({ 'erros': updateUsuario.getMsgError() });;


    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }
});

router.put('/usuarios/pessoas', verifyJWT, (req, res, next) => {
    try {

        let id = req.body.id;
        let email = req.body.email;
        let nomePessoa = req.body.nome;
        let sobrenomePessoa = req.body.sobrenome;
        let cpfPessoa = req.body.cpf;

        isEmpty(id) && res.status(400).json({ 'erro': `por favor envie o parametro como id:` })
        isEmpty(email) && res.status(400).json({ 'erro': `por favor envie o parametro como email:` })
        isEmpty(nomePessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{nome:}` })
        isEmpty(sobrenomePessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{sobrenome:}` })
        isEmpty(cpfPessoa) && res.status(400).json({ 'erro': `por favor envie o parametro como info_user:{cpf:}` })

        const updatePessoa = new Pessoa(id, nomePessoa, sobrenomePessoa, cpfPessoa, email);
        let confirma = updatePessoa.validacaoPessoa();
        confirma ? Pessoa.updatePes(res, updatePessoa) : res.status(200).json({ 'erros': updatePessoa.getMsgError() });;

    } catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }
});


router.get('/logout', verifyJWT, (req, res) => {

    try {
        res.status(200).json({ auth: false, token: null });
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/agendas', verifyJWT, (req, res, next) => {
    try {
        Agenda.getAllAgenda(res);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }
});

router.get('/agendas/:id', verifyJWT, (req, res, next) => {

    try {
        let id = parseInt(req.params.id);
        Agenda.getAgendaPorId(res, id);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/agendas/nome/:nome', verifyJWT, (req, res, next) => {

    try {
        let nome = req.params.nome;
        Agenda.getAgendaPorNome(res, nome);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/agendas/tipo/:tipo', verifyJWT, (req, res, next) => {

    try {
        let tipo = req.params.tipo;
        Agenda.getAgendaPorTipo(res, tipo);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/paginas', verifyJWT, (req, res, next) => {
    try {
        Pagina.getAllpage(res);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }
});

router.get('/paginas/:id', verifyJWT, (req, res, next) => {

    try {
        let id = parseInt(req.params.id);
        Pagina.getPagePorId(res, id);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/paginas/nome/:nome', verifyJWT, (req, res, next) => {

    try {
        let nome = req.params.nome;
        Pagina.getPagePorNome(res, nome);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/usuarios/pessoas', verifyJWT, (req, res, next) => {

    try {
        Pessoa.getAllPessoas(res);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/usuarios/pessoas/:id', verifyJWT, (req, res, next) => {

    try {
        let id = parseInt(req.params.id);
        Pessoa.getPessoasPorId(res, id);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/usuarios/pessoas/nome/:nome', verifyJWT, (req, res, next) => {

    try {
        let nome = req.params.nome;
        Pessoa.getPessoasPorNome(res, nome);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/usuarios', verifyJWT, (req, res, next) => {

    try {
        Usuario.getAllUsuarios(res);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/usuarios/:id', verifyJWT, (req, res, next) => {

    try {
        let id = parseInt(req.params.id);
        Usuario.getUsuarioPorId(res, id);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.get('/usuarios/login/:login', verifyJWT, (req, res, next) => {

    try {
        let login = req.params.login;
        Usuario.getUsuarioPorLogin(res, login);
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

router.delete('/usuarios', verifyJWT, (req, res, next) => {
    try {
        let id = req.body.id;
        isEmpty(id) && res.status(400).json({ 'erro': `por favor envie o parametro como id:` })

        const deleteUsuario = new Usuario(id, 'not', 'not', 'not', 'not@not.com', id, 'not', id, 0);

        let confirma = deleteUsuario.validacaoUser();
        confirma ? Usuario.deleteUser(res, deleteUsuario) : res.status(500).json({ 'erros': deleteUsuario.getMsgError() });;

    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }
});

router.delete('/agendas', verifyJWT, (req, res, next) => {
    try {
        let id = req.body.id;
        isEmpty(id) && res.status(400).json({ 'erro': `por favor envie o parametro como id:` })

        const deleteAgenda = new Agenda(id, 'not', 'not');

        let confirma = deleteAgenda.validacaoAgenda();
        confirma ? Agenda.deleteAgenda(res, deleteAgenda) : res.status(200).json({ 'erros': deleteAgenda.getMsgError() });;

    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }
});

router.delete('/paginas', verifyJWT, (req, res, next) => {
    try {
        let id = req.body.id;
        isEmpty(id) && res.status(400).json({ 'erro': `por favor envie o parametro como id:` })

        const deletePagina = new Pagina(id, 'not', 'not', id, id, 'not', 'not', '2019-01-01 00:00:00', '2019-01-01 00:00:00');

        let confirma = deletePagina.validacaoPagina();
        confirma ? Pagina.deletePage(res, deletePagina) : res.status(200).json({ 'erros': deletePagina.getMsgError() });;
    }
    catch (e) {
        res.status(500).json({ 'erro': 'não é possivel estabelecer conexão com o banco de dados' });
    }

});

export default router;