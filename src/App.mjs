import express from 'express';
import router from './routes/Routes';
import bodyParser from 'body-parser';
import dotenv from 'dotenv'


const port = process.env.PORT;
const app = express();

const service = () =>
  new Promise((res, rej) => {
    const start = Date.now();
    setTimeout(() => {
      const end = Date.now();
      res({
        ok: true,
        diff: end - start
      });
    }, 2000);
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,x-access-token');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use('/', router);

app.listen(port, () => console.log(`Servidor Rodando na porta ${port}`));
