import mysql from 'mysql';

class Connection{
    
    constructor(){
        this.host = process.env.HOST;
        this.port = process.env.PORTDB;
        this.user = process.env.USER;
        this.password = process.env.PASSWORD;
        this.database = process.env.DATABASE;
    }

    criarConexao(){
        
        return mysql.createConnection({
            'host': this.host,
            'port': this.port,
            'user': this.user,
            'password': this.password,
            'database': this.database
        });
    }

}

export default Connection;
