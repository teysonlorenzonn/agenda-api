import Connection from '../database/Connection.mjs';
import jwt from 'jsonwebtoken';
import Pagina from '../entity/Pagina.mjs';

class Consultas {


    static getUserJson(result) {
        let ok = [];
        result.forEach(item => {
            let tipo = false;
            item.ativo_usuario === 1 && (tipo = true);
            let json = {
                'id': item.id_usuario,
                'login': item.login_usuario,
                'aivo': tipo,
                'nivel': item.nivel_usuario,
                'info_user': {
                    'id': item.id_pessoa,
                    'nome': item.nome_pessoa + ' ' + item.sobrenome_pessoa,
                    'cpf': item.cpf_pessoa,
                    'email': item.email_pessoa
                }
            }

            ok.push(json);
        });
        return ok;
    }

    static getPessoaJson(result) {
        let ok = [];
        result.forEach(item => {
            let json = {
                'id': item.id_pessoa,
                'nome': item.nome_pessoa + ' ' + item.sobrenome_pessoa,
                'cpf': item.cpf_pessoa,
                'email': item.email_pessoa
            }

            ok.push(json);
        });
        return ok;
    }

    static getUserLoginJson(result, login, senha) {
        let loginAdk = '';
        let senhaAdk = '';
        let idAdk = 0;
        let ativoAdk = false

        result.forEach(item => {
            ativoAdk = false;
            item.ativo_usuario === 1 && (ativoAdk = true);

            idAdk = item.id_usuario;
            loginAdk = item.login_usuario;
            senhaAdk = item.senha_usuario;
        });

        let hasLogin = login === loginAdk;
        let hasSenha = senha === senhaAdk;


        if (hasLogin && hasSenha && ativoAdk) {
            //auth ok         
            let token = jwt.sign({ idAdk }, process.env.SECRET, {
                expiresIn: 172800 // expires in 2dias
            });

            return [200, { sessionId: idAdk, auth: true, token: token }];
        }


        if (!hasLogin || !ativoAdk) {
            return [200, { 'error': 'Email invalido' }];
        }
        return [200, { 'error': 'Senha invalida' }];
    }


    static getPaginaJson(result) {

        let ok = [];
        result.forEach(item => {
            let dtIni = new Date(item.data_inicial_pagina);
            let dtFini = new Date(item.data_final_pagina);
            let json = {
                'id': item.id_pagina,
                'nome': item.nome_pagina,
                'descricao': item.descricao_pagina,
                'data_inicial': dtIni,
                'data_final': dtFini,
                'info_agenda': {
                    'id': item.id_agenda,
                    'nome': item.nome_agenda,
                    'tipo': item.tipo_agenda,
                    'info_user': {
                        'id': item.id_usuario,
                        'login': item.login_usuario
                    }

                }
            }

            ok.push(json);
        });
        return ok;
    }

    static getAgendaJson(result) {
        let ok = [];
        result.forEach(item => {
            let json = {
                'id': item.id_agenda,
                'nome': item.nome_agenda,
                'tipo': item.tipo_agenda,
                'info_user': {
                    'id': item.id_usuario,
                    'login': item.login_usuario
                }
            }

            ok.push(json);
        });
        return ok;
    }

    static getAll(res, consulta, tipo, login, senha) {

        let newConn = new Connection();
        let conn = newConn.criarConexao();
        conn.connect();
        let pr = new Promise((resolve, reject) => {
            
            conn.query(consulta, (err, result, fields) => {
                if (err) reject(err);
                else resolve(result);
            });
        })
            .then(result => {
                switch (tipo) {
                    case 'user':
                        res.status(200).json(this.getUserJson(result));
                        break;
                    case 'user-login':
                        let retorno = this.getUserLoginJson(result, login, senha);
                        res.cookie('autenticado', retorno[1].auth)
                            .cookie('token', retorno[1].token)
                            .status(retorno[0])
                            .json(retorno[1]);

                        this.getUserLoginJson(result, login, senha, res);
                        break;
                    case 'pessoa':
                        res.status(200).json(this.getPessoaJson(result));
                        break;
                    case 'pagina':
                        res.status(200).json(this.getPaginaJson(result));
                        break;
                    case 'agenda':
                        res.status(200).json(this.getAgendaJson(result));
                        break;
                }
                conn.end();
            })
            .catch(reject => {
                res.status(200).json({ 'error': reject.sqlMessage });
                conn.end();
            });
    }

    static postAll(res, consulta, quantQuery, tipo) {
        let newConn = new Connection();
        let conn = newConn.criarConexao();
        let count = 0;
        conn.connect();
        let pr = new Promise((resolve, reject) => {
            while (quantQuery > 0) {
                conn.query(consulta[count], (err, result, fields) => {
                    if (err) reject(err);
                    else resolve(result);
                });
                count++;
                quantQuery--;
            }
        }).then(resolve => {
            switch (tipo) {
                case 'user':
                    res.status(200).json({ 'sucesso': 'usuario cadatrado com sucesso' });
                    break;
                case 'pagina':
                    res.status(200).json({ 'sucesso': 'pagina cadatrada com sucesso' });
                    break;
                case 'agenda':
                    res.status(200).json({ 'sucesso': 'agenda cadatrada com sucesso' });
                    break;
            }
            conn.end();
        }).catch(reject => {
            res.status(200).json({ 'error': reject.sqlMessage });
            conn.end();
        });
    }

    static putAll(res, consulta, quantQuery, tipo) {
        let newConn = new Connection();
        let conn = newConn.criarConexao();
        let count = 0;
        conn.connect();
        let pr = new Promise((resolve, reject) => {
            while (quantQuery > 0) {   
                conn.query(consulta[count], (err, result, fields) => {
                    if (err) reject(err);
                    else resolve(result);
                });
                count++;
                quantQuery--;
            }
        }).then(result => {
            switch (tipo) {
                case 'user':
                    res.status(200).json({ 'sucesso': 'update de usuario com sucesso' });
                    break;
                case 'pessoa':
                    res.status(200).json({ 'sucesso': 'update de pessoa com sucesso' });
                    break;
                case 'pagina':
                    res.status(200).json({ 'sucesso': 'update de pagina com sucesso' });
                    break;
                case 'agenda':
                    res.status(200).json({ 'sucesso': 'update de agenda com sucesso' });
                    break;
            }
            conn.end();
        }).catch(reject => {
            res.status(200).json({ 'error': reject.sqlMessage });
            conn.end();
        });
    }

    static deleteAll(res, consulta, quantQuery, tipo) {
        let newConn = new Connection();
        let conn = newConn.criarConexao();
        let count = 0;
        conn.connect();
        let pr = new Promise((resolve, reject) => {
            
            while (quantQuery > 0) {
                conn.query(consulta[count], (err, result, fields) => {
                    if (err) reject(err);
                    else resolve(result);
                });
                count++;
                quantQuery--;
            }
        }).then(resolve => {
            switch (tipo) {
                case 'user':
                    res.status(200).json({ 'sucesso': 'usuario deletado com sucesso' });
                    break;
                case 'pagina':
                    res.status(200).json({ 'sucesso': 'pagina deletada com sucesso' });
                    break;
                case 'agenda':
                    res.status(200).json({ 'sucesso': 'agenda deletada com sucesso' });
                    break;
            }
            conn.end();
        }).catch(reject =>{
            res.status(200).json({ 'error': reject.sqlMessage });
            conn.end();
        });
    }
}

export default Consultas